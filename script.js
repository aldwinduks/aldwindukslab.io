const hamburgerToggle = document.querySelector('.nav_toggle_button');
const nav = document.querySelector('.nav');

hamburgerToggle.addEventListener('click', () => {
    nav.classList.toggle('nav--visible');
})
